# Module: 1:
- Q.1.Why do we use virtualization and remote connections?
  - A A virtual desktop or virtual desktop infrastructure (VDI) means that a user’s __desktop environment is stored remotely on a server__, rather than on a local PC or other client computing device. Desktop virtualization software separates the desktop operating systems, applications and data from the hardware client, storing this “virtual desktop” on a remote server.
- Q.2. What are the advantages of installing a virtual machine locally instead of connecting to it remotely?
  - A remote connection might have 1) bottlenecks: Depending on how many are trying to access the systems. 2) Dependent on the third party connections.
- Q.3 How can you remotely connect to a Linux or Windows machine?
  - Using SSH tunneling, x11 forwarding
- Q.4 There’s a small issue with all virtualizations and remote connections. What is it?
  -  Scalability issue. Using multiple systems in the same hardware.
    - __JK:__ What do you mean by that?
