# Module 2:

## Linux Shell and File permissions
- Q1. Name commands used for: copying, moving and deleting files, changing directories, making new directories.
  - cp, mv, rm, cd, mkdir
- Q2. If your current directory is /home/ljubodoma/dnevnaSoba/, what is the absolute path of ../../slascicarna/?
  - absolute path is the path from the / direc.
  - /home/ljubodoma/dnevnaSoba/../../slascicarna
- Q3. Which directory does ~ refer to?
  - home
- Q4. Which directory contains system settings?
  - under etc
- Q5. What is autocomplete?
  - tab
- Q.6 How do you run an executable file in the current directory?
  - After changing the permission we use ./filename to run an executable.
- Q.7. Why and how would you use output redirection?
  - Would use output re-direction to redirect the output to some other device/file instead of the screen. STDIN--Keyboard STDOUT--screen. >
  - __JK *__ Not cleaned up
- Q.8. How would you execute a command within a command?
  - Does this mean a command within a shell script? OR $echo var_another_var
  - __JK *__ Not cleaned up
- Q.9. What is the purpose of the super user (root)?
  - For adminstrative  tasks.
  - __JK *__ Not cleaned up
- Q.10. You are user mkobal, member of the group godlike. What permissions do you have on the files listed below? Why?
  - After the read and write privileges comes the owner and then the group.
  - Already discussed till this point.
  - __JK *__ Not cleaned up

## Networking:
1. Is the computer with the IP 10.5.2.1, netmask 255.255.255.0 connectable to the IP 10.5.3.8? Why? What would happen if the netmask on both computers was 255.255.252.0?
  - Not sure.
  - __JK *__ No answer
2. What is a TCP/IP port? How many ports are there?
  - A port is a communication end point. System wise it is a logical construct.
  - __JK *__ No
3. What is NAT? What is a firewall? How do they relate?
  - Network Address Translation (NAT) is the process where a network device, usually a firewall, assigns a public address to a computer (or group of computers) inside a private network.
  - __JK *__ What is a firewall?
4. Why and how would you use SSH port tunneling?
  - remote connection.
5. Is www.example.com always the same as example.com? What's the difference to http://www.example.com?
  - One has a subdomain prepended to it and the other doesn't. Technically,  example.com and http://www.example.com are different domain names. They might not always be the same. In the second case an http command is being sent to the web server directing it to fetch and transmit the web page.
  - __JK *__ Are they really different domain names
  - __JK *__ What is HTTP?

## Software Installation:
1. What is a software package? Name a few package managers.
  - Assemblage of files that are developed to perform a particular task. Yum.
2. Which commands do you need to run to compile and install a downloaded source archive file (.tar.gz)?
  - untar and unzip the file and then configure the files after that use make to compile the files and then install.
  - __JK *__ Can you write the commands out?
3. What do you do when something goes wrong and compilation fails?
  - Try to find out what went wrong, do a error logging.
  - __JK *__ Explain
4. What went wrong during the compilation below?
  - Need to install re2c first.
