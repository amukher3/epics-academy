# Module 4

## Questions

1. What is "tech-talk"?
  - The main EPICS mailing list is called Tech-talk, and is used for technical discussions, questions and announcements.
2. Where can I find everything related to EPICS?
  - Manual and dev. guide
  - __JK *__: also epics website, tech-talk
3. What is the difference between a PV and a record?
  - PVs are the unique string address associated with the record. The default field is the name of the record. For ex, $(user):T_room
  - __JK *__: What do you mean by the default field
4. Each record has many PVs. What is the default PV associated with a record?
  - The default PV associated with a record is the VAL field.
5. What happens to record fields when the record processes?
  - Some of them are used and some of them are updated to run the IOC.
  - __JK *__: explain
6. What does the EPICS logo symbolize?
  - The EPIC logo symbolizes the mission and people of the El Paso Intelligence Center.
  - __JK *__: explain
7. What do you call the entity that reads or writes to a PV from a remote machine?
  - CA client.
8. Name a few shell commands that use "Channel Access".
  - caput, camonitor, caget
9. Which of the following hosts PVs, CA server or CA client?
  - CA server
10. What are CA servers called in EPICS?
  - IOCs
11. Name a few CA clients.
  - They are the OPIs -- First BOY,MEDM,EDM
12. What is the difference between an EPICS application and an IOC?
  - An EPICS application is the interaction of many IOCs.
  - __JK *__: explain
13. Which file do you usually need to modify before compiling EPICS software?
  - Makefile
  - __JK *__: which makefile? only makefile?
