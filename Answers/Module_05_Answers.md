# Module 5

## Questions

1. What is the purpose of makeBaseApp.pl?
  - to create epics appliations. It creates files and directories needed for the app.
2. What does a directory structure of an application look like? What is the purpose of different directories?
  - It has many directories and many folders. bin, configure,db,dbd etc. Different directories have different files which interact with each other to run the app.
  - __JK *__: explain in detail the purpose of each directory
3. Where do you place EPICS database files?
  - db directory
  - __JK *__: explain
4. What are substitution files and how to use them?
  - subsutitution files are like place holders used to substitute a value during run time. They are used as macros. The values written in them are substitute in the record during run time. The user.substitution files are in the Db folder.
5. How do you include EPICS modules?
  - In the Makefiles.
  - __JK *__: which Makefile
6. How do you include C code?
  - By making changes in the Makefile
7. Which directories are required at runtime?
  - Db,dbd, iocBoot containing the start up script.
  - __JK *__: aything else?
8. How do you build an EPICS application?
  - makeBaseApp.pl
  - __JK *__: build, not create
9. What is the location of st.cmd and how do you run an IOC application?
  - iocBoot.  ./ st.cmd. After making changes in the makefile and changing the permission of st.cmd we run st.cmd.
  - __JK *__: the path is weird, explain
  - __JK *__: is there another way to run the ioc?
10. What is the difference between /db and /<App.name>App/Db directories?
  - /db is created after compiling(running make) it just contains the record instances and the substitution files. /Db is created after we use build , it is where we place our record instances, it has the makefile that is used for including the record instances and the substitution templates.
  - __JK *__: explain
11. What happens if you make changes directly in the db directory? Do you need to compile the application for the changes to take affect?
  - the files in the /db folder are read only. They can't be executed.
  - __JK *__: explain
12. How do you get a list of all ai records in an application with descriptions.
  - dbl
  - __JK *__: with descriptions!!!
13. How can you read the time when the record was last processed?
  - The field Time in the record the filed can then be accessed using dbl or dbgf.
  - __JK *__: what if you don't have access to the IOC?
14. How can you read array PVs, how can you set their values?
  - For setting values: caput -a [options] <PV name> <no of elements> <value> ...,For reading/printing: use caget
15. How can you determine the machine where a specific record is available?
  - Not sure. The closeset seems to be the channel acces link for a particular record:dbel. This produces the channel access event list for a particular record.
  - __JK *__:
16. What is the VDCT debug plugin?
  - added on software to do debugging.
17. What types of record scanning are possible?
  - passive, periodic, event,I/o event,passive,Scan once.
  - __JK *__: What do you mean by Scan once?
18. How do you import custom dbd files in VDCT?
  - For loading a custom database defination file, "test.dbd" we write "java -jar VisualDCT.jar -DVDCT_DIR=~/epics test.dbd test.db" can also be imported from the "import module" option under "file" option in the top toolbar.
19. What is the difference between db and dbd files?
  - db are record instances but are read only. dbd are data base definations files.
20. What are links? What is the difference between various types of links? Which attributes of links can you set?
  - Links are like connections that are made to different types of records while they are being processed. These connections together help establish a certain kind of task. Difference between the various types of links lies in the way they are processed and whether they are input links or output links. For ex, PP and NPP.
  - __JK *__: explain
21. Explain the difference between setups on the following pictures?
  - Differences:
    - Apparent ones:
      - The ai record in the first setup has its scan set to periodic while the ai record in the second setup has its scan as passive.
      - In the first setup, the ai record has a forward link to the calc record in the second setup there isn't any such setup.
      - The calc record in the first setup has a forward link wheras the calc record in the second setup doesn't have a flnk. The inp link in the calc record for the first setup is NPP but the inp link in the second setup is PP.
      - The ao record in the first setup has its scan set to passive and the ao record in the second setup has its scan set to be periodic. The inp link in the ao record of the first set up has its processing set to non-passive whereas in the second setup the input link is process-passive.
    - Actually, the two setups are probably doing the same thing albeit in different ways. The second one seems to be a better implementation because in the first setup the ai record is processed twice. Firstly with the FLNK and the secondly calc writes the value because the inp link to calc has been set to NPP.
    - __JK *__: Probably doing the same thing?
    - __JK *__: Why is the record in first setup processed twice?
