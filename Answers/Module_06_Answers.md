# Module 6

## Questions

1. Are there any guidelines that must be followed for GUI design?
  - Yes
2. How can I change display defaults?
  - If by display defaults they mean the display.opi , the schema, then it can be changed by adding widgets OR click CSS --> click Display-->click OPI editor perspective
3. How to snap objects to grid?
  - Snap to Grid can be implemented from the toolbar OR from the "properties" pallete under "display" category.
4. How to align objects?
  - Objects can be aligned by selecting the "align middle","align left","align right" options in the top toolbar.
5. How to open other screens?
  - By screen I am assuming window. A new window can be opened by clicking on the "window" button on the top and then selecting "new window".
  - __JK *__: explain
6. How to execute shell commands?
  - Not sure. Shell commands can be executed from the epics shell.
  - __JK *__: from screen on the system running CSS?
7. How to change a display according to a PV's value or using a menu?
  - Display can be changed from the "properties" pallete on the right side.
8. How to display alarms?
  - For the old VM: CSS-->Alarms-->Alarm Table For the new VM: when a PV is connected and running the alarm statuses are shown in the properties pallete.
9. How to define macros?
  - Macros can be defined in the preference page OR they can be defined from the "properties" pallete OR right clicking the widget and selecting macros.
10. How to change widget visibility according to a PV's value?
  - Widget visibility can be changed from the "properties" pallete under the "behaviour" category under the "Rules" sub category. click on the "rules" add the PV name from the shell in the boolean expression write an expression something like "pv0>5". This will make the widget invisible whenever the value of the PV is higher than 5.
11. Which PV data is available to Boy? How to get PV data from other machines?
  - The PV data from the archive system(engine).  An Archive Engine takes PV data samples from a front-end computer, for example from EPICS IOCs via Channel Access, and places them in some data storage. The storage is a relational database , from the RDB the data is presented to a CSS based OPI.(Refer chapter 10 of the CSS developer's guide. Chapter-10 is The Archive system.)
  - For EPICS record, the SCAN field in combination with the ADEL field of analog records determines when a new value is sent to the archive engine.
