# Module 7

## Questions

1. Which views are available in VDCT?
  - Level up, zoom in,zoom out, zoom selection,base view,toolbar,statusbar,navigator,show grid,snap to grid, settings.
2. How do you create a record in VDCT?
  - File menu-->New
  - __JK *__: any other way?
3. How do you change record type in VDCT?
  - using the "Morph record option" on the top.
4. How do you point a link to a desired record field?
  - Just click on the link and move the cross hair to the INP field of any other reocrd.
5. How do you change link properties?
  - After you select the record on the right side two columns open one can edit the various fields there by clicking and editing there. Link properties can be also changed in a similar manner.
6. What is the best way to enter a large number of signals?
  - The best way seems to be sofmodule in synApps help connect a large number of signals to the I/O field of a record. It uses an AND gate over a FPGA controlled I/O device that is controlled by switches which are basically PVs. For detailed refer :https://epics.anl.gov/bcda/synApps/softGlue/softGlueDoc.html
  - __JK *__: the question was for VDCT specifically
7. How do you change VDCT canvas size?
  - click on "view" on the top --> click on "compactify canvas/zoom in/zoom out"
8. How can you monitor PV values in VDCT?
  - Not sure. Probably, from the VAL field in the spreadsheet view.
9. What is the desired form of record names?
  - They are case sensitive. Record names can be 28 characters in size, can be a mix of upper and lower case characters, should be unique across all IOCs in the subnet
  - __JK *__: anything else?
10. What are the record fields that always need to be considered?
  - The fields that always need to be considered are the fields generally common to all the records. These are for input type of records: INP,DTYP,RVAL,VAL,SIMM,SIML,SVAL,SIOL,SIMS. For output type of records:OUT,DTYP,VAL,OVAL,RVAL,RBV,DOL,OMSL,OIF,SIMM,SIML,SIOL,SIMS,IVOA,IVOV.
  - __JK *__: explain
11. When to use macros and when PVs for configuration?
  - I am assuming by configuration here it means that they are asking about configuring the records i.e the database configuration file(db or template). The input fields in the records can be set either by macros or as PV.If there are many inputs within a record then they can be set using a macro.
  - Process variable is a unique string address specifying a particular record.For a particular record if we think that we don't need to monitor the input then we can directly set the input field to be a macro but if we think that the input variable needs to be monitored we can set it to be PV by making it come from record.For example, suppose the ai type record $(user):T_room takes the input temperature from a macro but then spits out the same value as the output of the record thereby making the room temperature a PV.
  - At the end of the hierarchical chain in the record interaction the input has to come from a macro i.e different records interact,process update but then the sensors measures the input temperature of the room and reports it to the IOC.
  - __JK *__: explain
