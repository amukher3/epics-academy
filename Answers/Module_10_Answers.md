# Module 10

## Questions

N.B: Application Makefile is the Makefile in AppName/src directory.

1. Which files do you need to modify in order to include SNL source into your app?
  - For a example type application that also uses the sequencer -- in the application Makefile, in the src directory and the config/RELEASE file.
  - __JK *__: explain
2. How would you check whether Sequencer was properly included into your EPICS app?
  - If I am understanding the question correctly, to verify if it has been included correctly I would type "seq"  and see if the sequencer is there or not or we can run a demo example as well. Demo example can be run from the examples/demo and run directory by typing demo.stcmd
3. Is it possible to use macros in SNL? How?
  - Yes, it is possible to use macros in the program. It can be done using assign statement. For ex, assign voltage to "{unit}:ai1" . The string within the curly braces is the name of a program parameter and the whole thing (the name and the braces) are replaced with the value of the parameter. So, in Sequencer macros are like placeholder for variables unlike EPICS only string can be passed as parameters for names , values can't be passed.
4. What is an event flag?
  - State sets within a program may be synchronized through the use of event flags. typically, one state set will set an event flag, and another state set will test that event flag within a transition clause.
5. Why would you use more than one state set in one SNL program?
  - If more than one state are needed then the SNL will have more than one state. For ex, the A/C may have cooling and heating both.
  - __JK *__: explain
6. What tools can you use in the EPICS shell to debug SNL problems?
  - Sequencer can be debugged using the sequencer query commands: seqShow--displays information on all running state programs, seqShow--displays detailed information on program, seqChanShow---displays information on all channels,
  - Is there anything you can use in SNL code to debug?
7. How do you escape to C code in SNL?
  - So, the SNC to C compiler compiles the SNL to C code the resulting C file can then be compiled with a C compiler. Because the SNL does not support the full C language, C code may be escaped in the program. The escaped code is not compiled by snc, instead it is literally copied to the generated C code. Escaped C code is written between % signs i.e %{ }%
