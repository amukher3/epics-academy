#!../../bin/linux-x86_64/DevSup1

## You may have to change DevSup1 to something else
## everywhere it appears in this file

< envPaths

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/DevSup1.dbd"
DevSup1_registerRecordDeviceDriver pdbbase

## Load record instances
dbLoadRecords("db/DevSupRecord.db","user=codac-dev")

cd "${TOP}/iocBoot/${IOC}"
iocInit

## Start any sequence programs
#seq sncxxx,"user=codac-dev"
